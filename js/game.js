var Bomberman = Bomberman || {};

var gameProperties = {
    canvasSize: 704,
    numberOfLives: 3
};

var cursors;
var spacebar;
var player;

Bomberman.game = new Phaser.Game(gameProperties.canvasSize, gameProperties.canvasSize, Phaser.AUTO, '');

Bomberman.game.state.add('boot', Bomberman.boot);
Bomberman.game.state.add('preload', Bomberman.preload);
Bomberman.game.state.add('menu', Bomberman.menu);
Bomberman.game.state.add('gameInProgress', Bomberman.gameInProgress);
Bomberman.game.state.add('gameFinished', Bomberman.gameFinished);

Bomberman.game.state.start('boot');
