var Bomberman = Bomberman || {};

var spacebarSwitch;
var counter;

const flameSpeed = 100;


Bomberman.Player = function (id, game, spriteName) {
    var self = this;

    self.id = id;
    self.game = game;

    self.x = 0;
    self.y = 0;

    self.cursor = {
        left: false,
        right: false,
        up: false,
        down: false,
        bomb: false
    };

    self.input = {
        left: false,
        right: false,
        up: false,
        down: false,
        bomb: false
    };

    self.numberOfLives = gameProperties.numberOfLives;
    self.availableBombs = 3;
    self.game = game;

    self.sprite = game.add.sprite(self.x, self.y, spriteName);

    self.game.physics.arcade.enable(self.sprite);
    self.sprite.body.collideWorldBounds = true;
    self.playerSpeed = 200;

    self.update = update;
    self.placeBomb = placeBomb;
    self.kill = kill;
    self.process = process;
    self.pushToServer = pushToServer;

    function kill() {
        self.sprite.destroy();
    }

    function placeBomb(x, y) {
        if (self.availableBombs > 0) {
            var bombId = generateId();
            new Bomberman.Bomb(bombId, x, y, self.game, self);
            self.availableBombs--;
        }
    }

    function generateId() {
        return Math.random().toString(36).substr(2, 9);
    }

    function process() {
        self.sprite.body.x = self.x;
        self.sprite.body.y = self.y;
    }

    function pushToServer(bombFlag) {
        var playersInfo = {
            id: this.id,
            x: this.sprite.x,
            y: this.sprite.y,
            bomb: bombFlag,
            //velocityX: this.shipSprite.body.velocity.x,
            //velocityY: this.shipSprite.body.velocity.y,
            left: this.input.left,
            right: this.input.right,
            up: this.input.up,
            down: this.input.down
        };
        sock.send(JSON.stringify(playersInfo));
    }

    function update() {

        self.input.left = cursors.left.isDown;
        self.input.right = cursors.right.isDown;
        self.input.up = cursors.up.isDown;
        self.input.down = cursors.down.isDown;
        self.input.bomb = spacebar.isDown;


        // temp
        //self.cursor.left = self.input.left;
        //self.cursor.right = self.input.right;
        //self.cursor.up = self.input.up;
        //self.cursor.down = self.input.down;
        //self.cursor.bomb = self.input.bomb;

        var inputChanged = (
            this.cursor.left != this.input.left ||
            this.cursor.right != this.input.right ||
            this.cursor.up != this.input.up ||
            this.cursor.down != this.input.down
            //|| this.cursor.bomb != this.input.bomb
        );

        counter++;
        //console.log(counter);
        if (inputChanged || counter % 10 == 0) {
            self.pushToServer(false);
        }


        self.sprite.body.velocity.x = 0;
        self.sprite.body.velocity.y = 0;

        if (self.cursor.left) {
            self.sprite.body.velocity.x -= self.playerSpeed;
        } else if (self.cursor.right) {
            self.sprite.body.velocity.x += self.playerSpeed;
        }

        if (self.cursor.up) {
            self.sprite.body.velocity.y -= self.playerSpeed;
        } else if (self.cursor.down) {
            self.sprite.body.velocity.y += self.playerSpeed;
        }

        if (self.input.bomb) {
            if (!spacebarSwitch) {
                //self.placeBomb();
                self.pushToServer(true);
                spacebarSwitch = true;
            }
        }
        if (spacebar.isUp) {
            spacebarSwitch = false;
        }
    }
};

Bomberman.Bomb = function (id, x, y, game, bomber) {
    var self = this;

    self.id = id;
    self.x = x;
    self.y = y;
    self.game = game;
    self.sprite = self.game.add.sprite(x, y, 'bomb');
    self.bomber = bomber;

    self.bombtween = self.game.add.tween(self.sprite.scale).to({x: 1.1, y: 1.1}, 500, Phaser.Easing.Quadratic.InOut)
        .to({x: 1.0, y: 1.0}, 500, Phaser.Easing.Quadratic.InOut)
        .to({x: 1.1, y: 1.1}, 500, Phaser.Easing.Quadratic.InOut)
        .to({x: 1.0, y: 1.0}, 500, Phaser.Easing.Quadratic.InOut)
        .to({x: 1.1, y: 1.1}, 500, Phaser.Easing.Quadratic.InOut)
        .to({x: 1.0, y: 1.0}, 500, Phaser.Easing.Quadratic.InOut);

    self.bombtween.onComplete.add(doBombExplode);
    self.bombtween.start();

    function doBombExplode(scaleobj, tween) {
        new Bomberman.Flame(self.game, self.x, self.y, "up").create();
        new Bomberman.Flame(self.game, self.x, self.y, "down").create();
        new Bomberman.Flame(self.game, self.x, self.y, "right").create();
        new Bomberman.Flame(self.game, self.x, self.y, "left").create();
        self.sprite.kill();
        bomber.availableBombs++;
    }


    //******** flame object start ********//

    Bomberman.Flame = function (game, x, y, direction) {
        Phaser.Sprite.call(this, game, x, y, 'flame');
        game.physics.enable(this, Phaser.Physics.ARCADE);
        game.add.existing(this);
        this.direction = direction;
    };

    Bomberman.Flame.prototype = Object.create(Phaser.Sprite.prototype);
    Bomberman.Flame.prototype.constructor = Bomberman.Flame;

    Bomberman.Flame.prototype.update = function () {

        game.physics.arcade.collide(this, blockedLayer);
        //console.log("insdie bomb: "+currentPlayer);
        game.physics.arcade.overlap(this, currentPlayer.sprite, overlapCallback, null);

        if (this.direction == "up") {
            this.body.velocity.y = -flameSpeed;
        } else if (this.direction == "down") {
            this.body.velocity.y = flameSpeed;
        } else if (this.direction == "right") {
            this.body.velocity.x = flameSpeed;
        } else if (this.direction == "left") {
            this.body.velocity.x = -flameSpeed;
        }

        function overlapCallback(flame, player) {
            console.log("kill-" + currentPlayer.id);

            sock.send("kill-" + currentPlayer.id);
        }

    };


    Bomberman.Flame.prototype.create = function () {

        var firetween = game.add.tween(this.scale).to({x: 1.1, y: 1.1}, 500, Phaser.Easing.Quadratic.InOut)
            .to({x: 1.0, y: 1.0}, 500, Phaser.Easing.Quadratic.InOut)
            .to({x: 1.1, y: 1.1}, 500, Phaser.Easing.Quadratic.InOut)
            .to({x: 1.0, y: 1.0}, 500, Phaser.Easing.Quadratic.InOut)
            .to({x: 1.1, y: 1.1}, 500, Phaser.Easing.Quadratic.InOut)
            .to({x: 1.0, y: 1.0}, 500, Phaser.Easing.Quadratic.InOut);

        firetween.onComplete.add(finish, this);
        firetween.start();

        function finish(scaleobj, tween, flame) {
            this.destroy();
        }
    };

    //******** flame object end ********//


};

