var Bomberman = Bomberman || {};

var sock;
//var url = 'http://localhost:8080/echo';
//var url = 'http://192.168.1.147:8080/echo';
var url = 'http://10.194.130.51:8080/echo';

var disconnectedPlayerId;

var players;
var id;
var enemyIds = [];
var currentPlayer;
var ready;
var finalText;
var isGameFinished = false;

Bomberman.preload = function () {
    var self = this;

    self.preload = preload;
    self.create = create;

    self.game = this.game;


    function preload() {

        this.add.sprite(this.game.world.centerX - 36, this.game.world.centerY, 'preloadbar');

        // load game assets
        this.load.image('backgroundTile', 'assets/sprites/Blocks/BackgroundTile.png');
        this.load.image('playerSprite1', 'assets/sprites/Creep/creep1.png');
        this.load.image('playerSprite2', 'assets/sprites/Creep/creep2.png');
        this.load.image('bombermanFront', 'assets/sprites/Bomberman/Front/Bman_F_f00.png');
        this.load.image('bomb', 'assets/sprites/Bomb/Bomb.png');
        this.load.image('flame', 'assets/sprites/Flame/Flame.png');

        // load map
        this.game.load.tilemap('tileMap', 'assets/map/map2.json', null, Phaser.Tilemap.TILED_JSON);
        this.game.load.image('tileSet', 'assets/map/bomberman_tileset.png');

        //    configure cursors
        cursors = this.game.input.keyboard.createCursorKeys();
        spacebar = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        //    configure websocket
        sock = new SockJS(url);

        // listening to socket
        sock.onopen = function () {
            console.log("connected to " + url);
        };

        sock.onclose = function () {
            console.log("connection closed to  " + url);
        };

        sock.onerror = function () {
            console.log("error " + url);
        };

        sock.onmessage = function (msg) {

            //console.log("msg received: " + msg.data);
            //handshake
            if (msg.data.charAt(0) === "h") {
                if (msg.data.charAt(1) === "1") {

                    // part 1 receiving player id from server
                    id = parseInt(msg.data.split("-")[1]);
                    console.log("assigning id " + msg.data.split("-")[1] + "from server");

                    players = {};

                    //initiate handshake part 2
                    sock.send('clientReady-' + id);
                    ready = true;

                } else if (msg.data.charAt(1) === "2") {
                    console.log("spawning enemies " + msg.data);
                    var enemyId = msg.data.split("-")[1];
                    if (enemyId != id) {
                        // part 2 spawning enemies
                        console.log("enemy added: " + enemyId);
                        //players[enemyId] = new Bomberman.Player(enemyId, self.game);
                        enemyIds.push(enemyId);
                    }
                }
            } else if (msg.data.charAt(0) === "r") {
                // remove disconnected ship
                disconnectedPlayerId = msg.data.split("-")[1];
                delete players[disconnectedPlayerId];
                console.log("removing player " + disconnectedPlayerId);

            } else if (msg.data.charAt(0) === "k") {
                // remove disconnected ship
                disconnectedPlayerId = parseInt(msg.data.split("-")[1]);
                console.log("removing player " + disconnectedPlayerId);
                finalText = "Good job! You won!";
                if (disconnectedPlayerId == id) {
                    finalText = "You lost!";
                }
                delete players[disconnectedPlayerId];
                isGameFinished = true;
            } else {
                var playersInfo = JSON.parse(msg.data);
                var extractedId = parseInt(playersInfo.id);

                //console.log(playersInfo);
                //console.log('extractedId: ' + extractedId);

                if (extractedId == id) {
                    currentPlayer.cursor.left = playersInfo.left;
                    currentPlayer.cursor.right = playersInfo.right;
                    currentPlayer.cursor.up = playersInfo.up;
                    currentPlayer.cursor.down = playersInfo.down;
                    if (playersInfo.bomb) {
                        currentPlayer.placeBomb(playersInfo.x, playersInfo.y);
                    }

                } else {
                    var enemy = players[extractedId];
                    if (enemy) {
                        enemy.x = playersInfo.x;
                        enemy.y = playersInfo.y;

                        if (playersInfo.bomb) {
                            enemy.placeBomb(playersInfo.x, playersInfo.y);
                        }
                        //if(enemy.cursor.bomb == true){
                        //
                        //    console.log("BOMB!!!!!!!!!!!!!!");
                        //
                        //    //enemy.placeBomb(playersInfo.x, playersInfo.y);
                        //}

                        //    players[extractedId].shipSprite.body.velocity.x = playersInfo.velocityX;
                        //    players[extractedId].shipSprite.body.velocity.y = playersInfo.velocityY;
                        //    //players[extractedId].cursor.left = playersInfo.left;
                        //    //players[extractedId].cursor.right = playersInfo.right;
                        //    //players[extractedId].cursor.up = playersInfo.up;
                        //    //players[extractedId].cursor.down = playersInfo.down;
                        //console.log("--- " + enemy.id);
                    }
                }

            }
        };

    }

    function create() {
        this.state.start('menu');
    }
};
