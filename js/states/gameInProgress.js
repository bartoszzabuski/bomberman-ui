var Bomberman = Bomberman || {};

var blockedLayer;
var map;

Bomberman.gameInProgress = function () {
    var self = this;

    self.create = create;
    self.update = update;

    function create() {

        this.game.world.setBounds(0, 0, gameProperties.canvasSize, gameProperties.canvasSize);

        map = this.game.add.tilemap('tileMap');
        map.addTilesetImage('bomberman_tileset', 'tileSet');

        var backgroundLayer = map.createLayer('background');
        blockedLayer = map.createLayer('blocks');

        map.setCollisionBetween(1, 100000, true, 'blocks');

        players[id] = new Bomberman.Player(id, self.game, 'playerSprite1');
        currentPlayer = players[id];
        for (var i = 0; i < enemyIds.length; i++) {
            players[parseInt(enemyIds[i])] = new Bomberman.Player(enemyIds[i], self.game, 'playerSprite2');
        }

        backgroundLayer.resizeWorld();

    }

    function update() {

        this.game.physics.arcade.collide(currentPlayer.sprite, blockedLayer);

        currentPlayer.update();
        for (var i = 0; i < enemyIds.length; i++) {
            if(players[parseInt(enemyIds[i])]) {
                players[parseInt(enemyIds[i])].process();
            }
        }

        if (isGameFinished) {
            this.state.start('gameFinished');
        }

    }

    function kill() {

        console.log('kill');
    }


};

