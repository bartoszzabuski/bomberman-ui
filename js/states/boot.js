var Bomberman = Bomberman || {};

Bomberman.boot = function () {
    var self = this;

    self.preload = preload;
    self.create = create;

    function preload() {
        this.load.image('preloadbar', 'assets/loading.png');
    }

    function create() {
        this.scale.pageAlignHorizontally = true;

        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        //this.load.text('level1', 'assets/map/map.json');

        this.state.start('preload');

    }
};
