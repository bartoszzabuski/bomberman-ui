var Bomberman = Bomberman || {};

Bomberman.gameFinished = function () {
    var self = this;

    self.preload = preload;
    self.create = create;

    function preload() {
        this.game.load.bitmapFont('desyrel', 'assets/fonts/desyrel.png', 'assets/fonts/desyrel.xml');
    }

    function create() {
        this.game.add.bitmapText(200, 100, 'desyrel', finalText, 32);
    }
};
