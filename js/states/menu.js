var Bomberman = Bomberman || {};

var counter = 0;

Bomberman.menu = function () {
    var self = this;

    self.preload = preload;
    self.update = update;

    function preload() {

        this.background = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'backgroundTile');

        //give it speed in x
        this.background.autoScroll(-20, 0);

        this.bombermanFront = this.add.sprite(this.game.world.centerX -32, this.game.world.centerY - 250, 'bombermanFront');

        //start game text
        var text = "waiting for players...";
        var style = { font: "30px Arial", fill: "#fff", align: "center" };
        var t = this.game.add.text(this.game.width/2, this.game.height/2, text, style);
        t.anchor.set(0.5);


    }

    function update() {
        //if(this.game.input.activePointer.justPressed()) {
        //    this.game.state.start('gameInProgress');
        //}
        if(enemyIds.length == 1) {
            this.game.state.start('gameInProgress');
        }
    }
};
